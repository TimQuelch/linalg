#ifndef LINALG_MATRIX_H
#define LINALG_MATRIX_H

#include <algorithm>
#include <memory>
#include <utility>

namespace linalg {
	template <class T>
	class matrix {
	public:
		matrix(unsigned side = 1)
		    : _rows(side)
		    , _cols(side)
		    , _data(size() ? new T[size()] : nullptr) {}

		matrix(unsigned rows, unsigned cols)
		    : _rows(rows)
		    , _cols(cols)
		    , _data(size() ? new T[size()] : nullptr) {}

		matrix(std::pair<unsigned, unsigned> shape)
		    : _rows(shape.first)
		    , _cols(shape.second)
		    , _data(size() ? new T[size()] : nullptr) {}

		matrix(const matrix& other)
		    : _rows(other._rows)
		    , _cols(other._cols)
		    , _data(new T[other.size()]) {
			std::copy(std::begin(other._data), std::end(other._data), std::begin(_data));
		}

		matrix(matrix&& other)
		    : _rows(other._rows)
		    , _cols(other._cols)
		    , _data(std::move(other._data)) {}

		matrix& operator=(matrix other) {
			std::swap(*this, other);
			return *this;
		}

		~matrix() = default;

		unsigned rows() const { return _rows; }
		unsigned cols() const { return _cols; }
		unsigned size() const { return _rows * _cols; }
		std::pair<unsigned, unsigned> shape() const { return std::make_pair(_rows, _cols); }

		T& el(unsigned row, unsigned col) {
			if (row >= _rows || col >= _cols) {
				throw std::invalid_argument("Invalid index");
			}
			return _data.get()[index(row, col)];
		}

		T get(unsigned row, unsigned col) const {
			if (row >= _rows || col >= _cols) {
				throw std::invalid_argument("Invalid index");
			}
			return _data.get()[index(row, col)];
		}

		void set(unsigned row, unsigned col, const T& value) {
			if (row >= _rows || col >= _cols) {
				throw std::invalid_argument("Invalid index");
			}
			_data.get()[index(row, col)] = value;
		}

		friend bool operator==(const matrix& lhs, const matrix& rhs) {
			if (lhs.shape() != rhs.shape()) {
				return false;
			}
			T* lhs_data = lhs._data.get();
			T* rhs_data = rhs._data.get();
			for (int i = 0; i < lhs.size(); i++) {
				if (*lhs_data++ != *rhs_data++) {
					return false;
				}
			}
			return true;
		}

		friend bool operator!=(const matrix& lhs, const matrix& rhs) { return !(lhs == rhs); }

		void fill(const T& value) {
			T* ptr = _data.get();
			for (int i = 0; i < size(); i++) {
				*(ptr++) = value;
			}
		}

		template <class Operation>
		matrix& element_transform(const matrix& rhs) {
			if (shape() != rhs.shape()) {
				throw std::invalid_argument("Matrix dimensions do not match");
			}
			std::transform(std::begin(_data),
			               std::end(_data),
			               std::begin(rhs._data),
			               std::begin(_data),
			               Operation());
			return *this;
		}

		matrix& operator+=(const matrix& rhs) { return element_transform<std::plus>(rhs); }
		matrix& operator-=(const matrix& rhs) { return element_transform<std::minus>(rhs); }
		matrix& operator*=(const matrix& rhs) { return element_transform<std::multiplies>(rhs); }
		matrix& operator/=(const matrix& rhs) { return element_transform<std::divides>(rhs); }
		matrix& operator%=(const matrix& rhs) { return element_transform<std::modulus>(rhs); }
		friend matrix operator+(matrix one, const matrix& two) { return one += two; }
		friend matrix operator-(matrix one, const matrix& two) { return one -= two; }
		friend matrix operator*(matrix one, const matrix& two) { return one *= two; }
		friend matrix operator/(matrix one, const matrix& two) { return one /= two; }
		friend matrix operator%(matrix one, const matrix& two) { return one %= two; }

	private:
		unsigned _rows;
		unsigned _cols;
		std::unique_ptr<T> _data;

		unsigned index(unsigned row, unsigned col) const { return row + col * _rows; }
	};
}

#endif
